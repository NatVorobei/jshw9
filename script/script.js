//1. HTML тег можна створити за допомогою document.createElement або insertAdjacentHTML
//2. Перший параметр вказує в якому місці має бути вставлений тег. beforebegin, beforeend, afterbegin, afterend
//3. За допомогою методу remove() можна видалити елемент зі сторінки

let numbers = ["1", "2", "3", "sea", "user", 23];
let words = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function getListFromArray(arr){

    let ul = document.createElement('ul');

    for(let i = 0; i < arr.length; i++){
        let li = document.createElement('li');
        li.appendChild(document.createTextNode(arr[i]));
        ul.appendChild(li);
    }

    document.body.appendChild(ul);    
}

getListFromArray(words);




